package mk.ukim.finki.akreditacii.service;

public interface ProfessorDeleteService {

    void deleteProfessor(String id);

}