package mk.ukim.finki.akreditacii.model.consultations;

public enum ConsultationStatus {
    ACTIVE, INACTIVE
}
