package mk.ukim.finki.akreditacii.model.room;

public enum RoomType {

    CLASSROOM, LAB, MEETING_ROOM, OFFICE, VIRTUAL
}
