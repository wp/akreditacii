package mk.ukim.finki.akreditacii.model.professor;

public enum EducationDegree {
    VI_GRADUATED, VII_MASTER, VIII_PHD
}
