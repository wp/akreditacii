package mk.ukim.finki.akreditacii.model.professor;

import jakarta.persistence.*;
import lombok.*;
import org.hibernate.Hibernate;

import java.util.Base64;
import java.util.Objects;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "professor_resume")
public class ProfessorResume {
    @Id
    @Column(name = "id")
    private String id;

    @ManyToOne
    @JoinColumn(name = "id", insertable = false, updatable = false)
    private Professor professor;

    @Column(name = "biography")
    private String resume;


    @Column(name = "image")
    private byte[] image;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        ProfessorDetails professor = (ProfessorDetails) o;
        return getId() != null && Objects.equals(getId(), professor.getId());
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }

    public String generateBase64Image(){
        return Base64.getEncoder().encodeToString(this.image);
    }

}
