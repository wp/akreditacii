package mk.ukim.finki.akreditacii.model;

public enum StudyCycle {

    UNDERGRADUATE, MASTER, PHD
}
