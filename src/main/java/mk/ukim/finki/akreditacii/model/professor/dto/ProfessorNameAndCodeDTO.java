package mk.ukim.finki.akreditacii.model.professor.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ProfessorNameAndCodeDTO {
    String code;
    String name;
}
