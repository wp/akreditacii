package mk.ukim.finki.akreditacii.model.semester;

public enum SemesterType {
    WINTER,
    SUMMER
}
