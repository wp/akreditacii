package mk.ukim.finki.akreditacii.model.consultations;

public enum ConsultationType {
    ONE_TIME, WEEKLY
}
