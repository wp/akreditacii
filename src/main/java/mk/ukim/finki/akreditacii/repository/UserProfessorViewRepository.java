package mk.ukim.finki.akreditacii.repository;

import mk.ukim.finki.akreditacii.model.UserProfessorView;

public interface UserProfessorViewRepository extends JpaSpecificationRepository<UserProfessorView, String> {

}
